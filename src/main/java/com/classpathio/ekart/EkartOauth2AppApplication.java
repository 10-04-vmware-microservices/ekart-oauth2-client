package com.classpathio.ekart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EkartOauth2AppApplication {

	public static void main(String[] args) {
		SpringApplication.run(EkartOauth2AppApplication.class, args);
	}

}
