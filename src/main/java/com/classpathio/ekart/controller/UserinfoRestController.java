package com.classpathio.ekart.controller;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/userinfo")
public class UserinfoRestController {
	
	private final OAuth2AuthorizedClientService oauth2ClientService;
	
	public UserinfoRestController(OAuth2AuthorizedClientService oAuth2AuthorizedClientService) {
		this.oauth2ClientService = oAuth2AuthorizedClientService;
	}
	
	@GetMapping
	public Map<String, String> userInfo(OAuth2AuthenticationToken oauth2Token){
		OAuth2AuthorizedClient oAuth2AuthorizedClient = this.oauth2ClientService
																	.loadAuthorizedClient(oauth2Token.getAuthorizedClientRegistrationId(), oauth2Token.getPrincipal().getName());
		Map<String, String> responseMap = new LinkedHashMap<>();
		if(! Objects.isNull(oAuth2AuthorizedClient)) {
			OAuth2AccessToken accessToken = oAuth2AuthorizedClient.getAccessToken();
			String tokenValue = accessToken.getTokenValue();
			String issuedAt = accessToken.getIssuedAt().atZone(ZoneId.systemDefault()).toLocalDate()
					.format(DateTimeFormatter.BASIC_ISO_DATE).toString();
			String expiresAt = accessToken.getExpiresAt().atZone(ZoneId.systemDefault()).toLocalDate()
					.format(DateTimeFormatter.BASIC_ISO_DATE).toString();
			String scopes = accessToken.getScopes().toString();

			responseMap.put("access-token", tokenValue);
			responseMap.put("issued-at", issuedAt);
			responseMap.put("expires-at", expiresAt);
			responseMap.put("scopes", scopes);
		}
		return responseMap;
	}

}
