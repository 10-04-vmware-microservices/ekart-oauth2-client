package com.classpathio.ekart.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class WebApplicationSecurityConfiguration extends WebSecurityConfigurerAdapter{

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.cors().disable();
		http.csrf().disable();
		http.authorizeRequests()
		       .antMatchers("/login/**", "/logout/**", "/contact-us/**")
		          .permitAll()
		       .anyRequest()
		           .authenticated()
		       .and()
		           .oauth2Login()
		       .redirectionEndpoint()
		          .baseUri("/authorization-code/callback");

	}
}
